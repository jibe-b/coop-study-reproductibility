# Rapport enquête Revenus-Temps (Coopaname et Oxalis) et reproductibilité

## Description

Les données du _Rapport enquête Revenus-Temps (Coopaname et Oxalis)_ sont mises à disposition au format pdf. Ce projet vise à les convertir vers un format interopérable et à réaliser une nouvelle analyse de ces données (suivant l'analyse réalisée du rapport) sous la forme d'un notebook exécutable afin de gagner en reproductibilité.

## Productions

Les données résultantes sont présentes dans le [notebook dérivé](https://gitlab.com/jibe-b/coop-study-reproductibility/blob/master/coop-study-reproducibility.ipynb).

Une trace du processus suivi pour l'ouverture des données est présente dans le [notebook sur l'ouverture de données](https://gitlab.com/jibe-b/coop-study-reproductibility/blob/master/open-data-process.ipynb)

## Pipeline

- [tabula](http://tabula.technology/) pour extraire les données du pdf présentes sous forme de tableau
- pdftotext pour extraire le texte des paragraphes
- jupyter notebook pour agréger les textes et les tableaux


## Background

Ce projet est réalisé par [jibe_jeybee](https://twitter.com/jibe_jeybee) en parallèle de l'exploration des Coopératives d'Activité et d'Emploi (CAE) comme cadre pour l'exercice d'activités de recherche ([en savoir plus](https://gitlab.com/jibe-b/my-own-coop/issues)) ainsi qu'en parallèle de la mise en place de mon activité en indépendant autour de l'ouverture de données de recherche et leur usage résultant ([en savoir plus](https://gitlab.com/jibe-b/open-data-analyst))

## Contribution

Tout contribution est la bienvenue, à commencer par les questions, suggestions, commentaires via le [gestionnaire d'issues](https://gitlab.com/jibe-b/coop-study-reproductibility/issues) ainsi que de pull requests.

## "Propriété" intellectuelle

La licence des données de l'étude originale n'est pas explicitée donc par défaut, ces données sont "tous droits réservés, Manucoop".

Le code original produit au cours de ce projet est sous licence GPL3 et les contenus sont sous licence CC-BY.

## Bonnes pratiques

- l'intégralité de ce projet est versionné depuis la création du README
- l'intégralité des étapes computationnelles seront reproductibles et fournies sous la forme d'un docker
- l'intégralité des calculs réalisés seront présents dans un notebook exécutable
- le choix de licences libres pour le code et les contenus produits, ainsi que l'invitation sincère et engagée à la coopération avec les personnes souhaitant contribuer font également partie, implicitement, des bonnes pratiques
